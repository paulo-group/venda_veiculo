﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VendaVeiculo.API.Models;

namespace VendaVeiculo.API.Persistence.Interfaces
{
    public interface IPropostaRepository
    {
        Task<IList<Proposta>> Propostas();
        Task<Proposta> AddProposta(Proposta proposta);
        Task<Proposta> ObterPorId(Guid id);
        Task<Proposta> Atualizar(Proposta proposta);
        Task Excluir(Proposta proposta);
    }
}