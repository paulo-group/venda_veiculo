﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace VendaVeiculo.API.Models
{
    [Table("Propostas")]
    public class Proposta
    {
        public Proposta()
        {
            Veiculo = new Veiculo();
        }
        public Guid Id { get; set; }
        public string NomeCliente { get; set; }
        public DateTime DataProposta { get; set; }
        public decimal Valor { get; set; }

        public Guid IdVeiculo { get; set; }
        public virtual Veiculo Veiculo { get; set; }
    }
}
