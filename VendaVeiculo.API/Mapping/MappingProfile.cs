﻿using AutoMapper;
using VendaVeiculo.API.Controllers.Resources;
using VendaVeiculo.API.Models;

namespace VendaVeiculo.API.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Domínio para o Recurso da API
            CreateMap<Veiculo, VeiculoResource>();
            CreateMap<Proposta, PropostaResource>();


            // Recurso da API para o domínio

            CreateMap<VeiculoResource, Veiculo>();
            CreateMap<PropostaResource, Proposta>();
        }
    }
}
