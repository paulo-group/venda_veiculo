﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendaVeiculo.API.Models;

namespace VendaVeiculo.API.Controllers.Resources
{
    public sealed class PropostaResource
    {
        public PropostaResource()
        {
            Veiculo = new Veiculo();
        }

        public Guid Id { get; set; }
        public string NomeCliente { get; set; }
        public DateTime DataProposta { get; set; }
        public decimal Valor { get; set; }

        public Guid IdVeiculo { get; set; }
        public Veiculo Veiculo { get; set; }
    }
}
