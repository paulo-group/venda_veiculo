import { Veiculo } from './veiculo';

export class Proposta {
  id: string;
  dataProposta: string;
  idVeiculo: string;
  valor: number;
  nomeCliente: string;
  veiculo: Veiculo;
}
