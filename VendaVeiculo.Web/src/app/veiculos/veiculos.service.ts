import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Veiculo } from '../shared/models/veiculo';
import { APP_API } from '../app.api';

@Injectable()

export class VeiculosService {

  constructor(private http: HttpClient) {}

  veiculos(): Observable<Veiculo[]> {
    return this.http.get<Veiculo[]>(`${APP_API}/veiculos`);
  }

  veiculoPorId(id: string): Observable<Veiculo> {
    return this.http.get<Veiculo>(`${APP_API}/veiculos/${id}`);
  }

  addVeiculo(veiculo: Veiculo): Observable<Veiculo> {
    return this.http.post<Veiculo>(`${APP_API}/veiculos`, veiculo);
  }

  atualizar(veiculo: Veiculo): Observable<Veiculo> {
    return this.http.put<Veiculo>(`${APP_API}/veiculos`, veiculo);
  }

  deletar(id: string) {
    return this.http.delete<Veiculo>(`${APP_API}/veiculos/${id}`);
  }
}
