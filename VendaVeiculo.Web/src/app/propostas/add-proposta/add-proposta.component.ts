import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PropostasService } from '../propostas.service';
import { Proposta } from 'src/app/shared/models/proposta';
import { VeiculosService } from 'src/app/veiculos/veiculos.service';
import { Veiculo } from 'src/app/shared/models/veiculo';
import { catchError, retry } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-add-proposta',
  templateUrl: './add-proposta.component.html'
})
export class AddPropostaComponent implements OnInit {

  formCadastar: FormGroup;
  proposta: Proposta;
  propostaRetorno: Proposta;
  idProposta: string;
  veiculo: Veiculo;
  idVeiculo: string;

  constructor(
    private formBuilder: FormBuilder,
    private propostasService: PropostasService,
    private veiculosService: VeiculosService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.idVeiculo = this.route.snapshot.params.id;

    this.veiculosService.veiculoPorId(this.idVeiculo)
    .subscribe(veiculo => this.veiculo = veiculo);
  }

  ngOnInit() {


    this.formCadastar = this.formBuilder.group({
      nomeCliente: ['', Validators.required],
      valor: ['', Validators.required],
    });
  }

  cadastrarProposta() {
    this.formCadastar.value.idVeiculo = this.idVeiculo;
    this.formCadastar.value.veiculo = null;

    this.propostasService
      .addProposta(this.formCadastar.value)
      .pipe(
        retry(1),
        catchError(this.handleError)

      )
      .subscribe(() => {
        this.router.navigate(['/veiculos/detalhes', this.veiculo.id]);
      });
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.resposta[0]}`;
    } else {
      // server-side error
      errorMessage = `${error.error.resposta[0]}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}
